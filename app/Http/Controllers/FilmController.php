<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function index()
    {
        $films = Film::all();
        return view('films.index', compact('films'));
    }

    public function show($id)
    {
        $film = Film::findOrFail($id);
        $genreName = $film->genre->name;
        $reviews = $film->reviews;
        return view('films.show', compact('film', 'genreName', 'reviews'));
    }

    public function create()
    {
        $genres = Genre::all();
        return view('films.create', compact('genres'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'genre_id' => 'required|exists:genres,id',
        ]);

        Film::create([
            'title' => $request->title,
            'description' => $request->description,
            'genre_id' => $request->genre_id,
        ]);

        return redirect('films')
            ->with('success', 'Film added successfully');
    }

    public function edit($id)
    {
        $genres = Genre::all();
        $film = Film::findOrFail($id);
        return view('films.edit', compact(['film', 'genres']));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'genre_id' => 'required|exists:genres,id',
        ]);

        $film = Film::findOrFail($id);
        $film->update([
            'title' => $request->title,
            'description' => $request->description,
            'genre_id' => $request->genre_id,
        ]);

        return redirect('films')
            ->with('success', 'Film updated successfully');
    }

    public function destroy($id)
    {
        $film = Film::findOrFail($id);
        $film->delete();

        return redirect('films')
            ->with('success', 'Film deleted successfully');
    }

    public function showReviews(Film $film)
    {
        $reviews = $film->reviews;

        return view('films.reviews', compact('film', 'reviews'));
    }

    public function createReviewForm($id)
    {
        $film = Film::findOrFail($id);
        return view('reviews.create', compact('film'));
    }

    public function storeReview(Request $request)
    {

        Review::create([
            'film_id' => $request->film_id,
            'user_id' => $request->user_id,
            'comment' => $request->comment,
            'rating' => $request->rating,
        ]);

        return redirect('/films/show/' . $request->film_id)
            ->with('success', 'Review added successfully');
    }
}
