@extends('layouts.app')

@section('content')
    <div class="container bg-white p-3">
        <h2>Edit Film</h2>
        <form action="{{ route('films.update', $film->id) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group mt-3">
                <label for="title">Title:</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $film->title }}" required>
            </div>

            <div class="form-group mt-3">
                <label for="description">Description:</label>
                <textarea class="form-control" name="description" id="description" rows="4" required>{{ $film->description }}</textarea>
            </div>

            <div class="form-group mt-3">
                <label for="genre_id">Genre:</label>
                <select class="form-control" name="genre_id" id="genre_id" required>
                    @foreach ($genres as $genre)
                        <option value="{{ $genre->id }}" {{ $film->genre_id == $genre->id ? 'selected' : '' }}>
                            {{ $genre->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary mt-3">Update Film</button>

        </form>
    </div>
@endsection
