@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $film->title }}</div>

                    <div class="card-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Description</th>
                                    <td>{{ $film->description }}</td>
                                </tr>
                                <tr>
                                    <th>Genre</th>
                                    <td>{{ $genreName }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col">
                                <h3>Reviews</h3>
                            </div>
                            <div class="col">
                                <a href="/films/add-review/{{ $film->id }}"
                                    class="btn btn-primary btn-sm float-end">Buat
                                    Review</a>
                            </div>
                        </div>

                        @if (count($reviews) > 0)
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Comment</th>
                                        <th>Rating</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reviews as $review)
                                        <tr>
                                            <td>{{ $review->user->name }}</td>
                                            <td>{{ $review->comment }}</td>
                                            <td>{{ $review->rating }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <p>No reviews available.</p>
                        @endif

                        <p><a href="{{ url('/') }}">Back to List</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
