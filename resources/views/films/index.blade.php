@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>List of Films</h2>
            </div>
            @auth
                <div class="col">
                    <a href="/films/create" class="btn btn-primary float-end">+ Film</a>
                    <a href="/genres" class="btn btn-primary float-end me-2">+ Genre</a>
                </div>
            @endauth


        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Genre</th>
                        @auth
                            <th>Action</th>
                        @endauth
                    </tr>
                </thead>
                <tbody>
                    @foreach ($films as $film)
                        <tr>
                            <td>{{ $film->title }}</td>
                            <td>{{ $film->description }}</td>
                            <td>{{ $film->genre->name }}</td>
                            @auth
                                <td class="float-end">
                                    <a href="films/show/{{ $film->id }}" class="btn btn-primary btn-sm">View
                                        Details</a>
                                    <a href="films/edit/{{ $film->id }}" class="btn btn-warning btn-sm">Edit</a>
                                    <a href="films/destroy/{{ $film->id }}" class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah anda yakin?')">Hapus</a>
                                </td>
                            @endauth
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
