@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Genre</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('genres.update', $genre->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Genre Name:</label>
                                <input type="text" name="name" class="form-control" value="{{ $genre->name }}"
                                    required>
                            </div>
                            <button type="submit" class="btn btn-primary">Update Genre</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
