@extends('layouts.app')

@section('content')
    <div class="container bg-white p-3">
        <h2>Add Review for {{ $film->title }}</h2>
        <form action="/films/reviews/store" method="post">
            @csrf
            <input type="hidden" name="film_id" value="{{ $film->id }}">
            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

            <div class="form-group mt-3">
                <label for="comment">Comment:</label>
                <textarea class="form-control" name="comment" id="comment" rows="4" required></textarea>
            </div>

            <div class="form-group mt-3">
                <label for="rating">Rating:</label>
                <input type="number" class="form-control" name="rating" id="rating" min="1" max="5"
                    required>
            </div>

            <button type="submit" class="btn btn-primary mt-3">Submit Review</button>

        </form>
    </div>
@endsection
