<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('films');
});

Route::get('/home', function () {
    return redirect('films');
});

// Menampilkan halaman login
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

// Rute untuk Genre
Route::resource('genres', GenreController::class)->middleware('auth'); // CRUD for Genre
Route::get('genres', [GenreController::class, 'index'])->name('genres.index');
Route::get('genres/create', [GenreController::class, 'create'])->name('genres.create');
Route::post('genres', [GenreController::class, 'store'])->name('genres.store');
Route::get('genres/{genre}', [GenreController::class, 'show'])->name('genres.show');
Route::get('genres/{genre}/edit', [GenreController::class, 'edit'])->name('genres.edit');
Route::put('genres/{genre}', [GenreController::class, 'update'])->name('genres.update');
Route::delete('genres/{genre}', [GenreController::class, 'destroy'])->name('genres.destroy');

// Rute untuk Film
Route::resource('films', FilmController::class)->middleware('auth')->except(['index', 'show']);
Route::get('films', [FilmController::class, 'index']);
Route::get('films/create', [FilmController::class, 'create'])->name('films.create');
Route::get('films/show/{id}', [FilmController::class, 'show']);
Route::get('films/edit/{film}', [FilmController::class, 'edit'])->name('films.edit');
Route::put('films/update/{film}', [FilmController::class, 'update'])->name('films.update');
Route::get('films/destroy/{film}', [FilmController::class, 'destroy'])->name('films.destroy');
Route::get('films/reviews/{film}', [FilmController::class, 'showReviews'])->name('films.reviews');
Route::get('films/add-review/{id}', [FilmController::class, 'createReviewForm'])->name('films.create_review');
Route::post('films/reviews/store', [FilmController::class, 'storeReview']);
