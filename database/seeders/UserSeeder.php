<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Andi',
            'email' => 'andi@tes.com',
            'password' => Hash::make('1'),
        ]);

        User::create([
            'name' => 'Mila',
            'email' => 'mila@tes.com',
            'password' => Hash::make('1'),
        ]);

        User::create([
            'name' => 'Joni',
            'email' => 'joni@tes.com',
            'password' => Hash::make('1'),
        ]);

        User::create([
            'name' => 'Iqbal',
            'email' => 'iqbal@tes.com',
            'password' => Hash::make('1'),
        ]);

        User::create([
            'name' => 'Delia',
            'email' => 'delia@tes.com',
            'password' => Hash::make('1'),
        ]);
    }
}
